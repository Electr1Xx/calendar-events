import Vue from 'vue'
import App from './App.vue'
import moment from 'moment'
import data from './assets/events'


Vue.config.productionTip = false
Vue.prototype.moment = moment

Vue.filter('time', function (val) {
    var format = "DD/MM/YYYY HH:mm";

    if (moment(val).startOf('day').valueOf() === moment(val).valueOf()) {
        format = "DD/MM/YYYY"

    }
    return moment(val).format(format);

})

Vue.filter('type', function (val) {
    let types = data.meta.types
    types.forEach(function (type) {
            if (type[0] === val) {
                return val = type[1]
            }
        }
    )
    if (typeof val === 'number') {
        return val = 'no_type'
    } else {
        return val
    }
})


new Vue({
    render: h => h(App)
}).$mount('#app')
